let express = require('express');
let app = express();
let cmd = require('node-cmd');

//create an array of jokes
let jokes = [
  "Dad, did you get a haircut? No, I got them all cut!",
  "My wife is really mad at the fact that I have no sense of direction. So I packed up my stuff and right!",
  "How do you get a squirrel to like you? Act like a nut.",
  "Why don't eggs tell jokes? They'd crack each other up.",
  "I don't trust stairs. They're always up to something.",
  "What do you call someone with no body and no nose? Nobody knows.",
  "Did you hear the rumor about butter? Well, I'm not going to spread it!",
  "Why couldn't the bicycle stand up by itself? It was two tired.",
  "Dad, can you put my shoes on? No, I don't think they'll fit me.",
  "Why can't a nose be 12 inches long? Because then it would be a foot.",
  "This graveyard looks overcrowded. People must be dying to get in.",
  "Dad, can you put the cat out? I didn't know it was on fire.",
  "What time did the man go to the dentist? Tooth hurt-y.",
  "How many tickles does it take to make an octopus laugh? Ten tickles.",
  "What concert costs just 45 cents? 50 Cent featuring Nickelback!",
  "How do you make a tissue dance? You put a little boogie in it.",
  "Why did the math book look so sad? Because of all of its problems!",
  "What do you call cheese that isn't yours? Nacho cheese.",
  "What kind of shoes do ninjas wear? Sneakers!",
  "How does a penguin build its house? Igloos it together."
]
app.get("/", (req, resp)=>{
  //send out all the jokes
  resp.send(jokes);
});

//create the random route
app.get("/random", (req, resp)=>{
  let choice = Math.floor(Math.random() * jokes.length);
  resp.send({joke:jokes[choice], number:choice})
})

const port = process.env.PORT || 3000;
app.listen(port, ()=>{
  console.log(`Listening on port ${port}`);
  cmd.run(`open http://localhost:${port}`);
});
